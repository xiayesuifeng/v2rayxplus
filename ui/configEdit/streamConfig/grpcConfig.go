package streamConfig

import (
	"github.com/therecipe/qt/widgets"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
)

type GrpcConfig struct {
	*widgets.QWidget

	serviceNameLineEdit *widgets.QLineEdit
}

func NewGrpcConfig(parent widgets.QWidget_ITF) *GrpcConfig {
	widget := widgets.NewQWidget(parent, 0)

	grpcConfig := &GrpcConfig{QWidget: widget}
	grpcConfig.init()

	return grpcConfig
}

func (ptr *GrpcConfig) init() {
	formLayout := widgets.NewQFormLayout(ptr)
	formLayout.SetContentsMargins(0, 0, 0, 0)

	ptr.serviceNameLineEdit = widgets.NewQLineEdit(ptr)

	formLayout.AddRow3("serviceName", ptr.serviceNameLineEdit)

	ptr.SetLayout(formLayout)
}

func (ptr *GrpcConfig) saveConfig() *conf.GrpcConfig {
	name := ptr.serviceNameLineEdit.Text()
	if name != "" {
		return &conf.GrpcConfig{ServiceName: name}
	} else {
		return nil
	}
}
