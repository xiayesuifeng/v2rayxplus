package streamConfig

import (
	"encoding/json"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	"log"
)

type WebSocketConfig struct {
	*widgets.QWidget

	pathLineEdit    *widgets.QLineEdit
	headersTextEdit *widgets.QPlainTextEdit
}

func NewWebSocketConfig(parent widgets.QWidget_ITF) *WebSocketConfig {
	widget := widgets.NewQWidget(parent, 0)

	webSocketConfig := &WebSocketConfig{QWidget: widget}
	webSocketConfig.init()

	return webSocketConfig
}

func (ptr *WebSocketConfig) init() {
	formLayout := widgets.NewQFormLayout(ptr)
	formLayout.SetContentsMargins(0, 0, 0, 0)

	ptr.pathLineEdit = widgets.NewQLineEdit(ptr)
	ptr.headersTextEdit = widgets.NewQPlainTextEdit(ptr)

	ptr.pathLineEdit.SetPlaceholderText("/")
	ptr.headersTextEdit.SetPlaceholderText(`{
  "key": "value"
}`)

	formLayout.AddRow3("path", ptr.pathLineEdit)
	formLayout.AddRow3("headers", ptr.headersTextEdit)

	ptr.SetLayout(formLayout)
}

func (ptr *WebSocketConfig) saveConfig() *conf.WebSocketConfig {
	wsConfig := &conf.WebSocketConfig{}
	if path := ptr.pathLineEdit.Text(); path != "" {
		wsConfig.Path = path
	} else {
		return nil
	}

	if headers := ptr.headersTextEdit.ToPlainText(); headers != "" {
		headersMap := map[string]string{}
		if err := json.Unmarshal([]byte(headers), &headersMap); err != nil {
			log.Println("Failed to unmarshal headers, error:", err)
		} else {
			wsConfig.Headers = headersMap
		}
	}

	return wsConfig
}
