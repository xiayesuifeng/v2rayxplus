package widgets

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	"strconv"
	"strings"
)

type DNSListWidget struct {
	widgets.QWidget

	listView  *widgets.QListView
	listModel *DNSListModel

	addButton      *widgets.QPushButton
	copyButton     *widgets.QPushButton
	deleteButton   *widgets.QPushButton
	upMoveButton   *widgets.QPushButton
	downMoveButton *widgets.QPushButton

	advancedFrame *widgets.QFrame

	advancedCheckBox *widgets.QCheckBox

	portEdit      *widgets.QLineEdit
	clientIPEdit  *widgets.QLineEdit
	domainsEdit   *widgets.QLineEdit
	expectIPsEdit *widgets.QLineEdit

	currentDnsServer *conf.DnsServer

	_ func() `constructor:"init"`
}

func (ptr *DNSListWidget) init() {
	mainLayout := widgets.NewQVBoxLayout2(ptr)

	hboxLayout := widgets.NewQHBoxLayout2(ptr)

	ptr.listModel = NewDNSListModel(ptr)

	ptr.listView = widgets.NewQListView(ptr)
	ptr.listView.SetModel(ptr.listModel)

	ptr.addButton = widgets.NewQPushButton2("新建", ptr)
	ptr.copyButton = widgets.NewQPushButton2("复制", ptr)
	ptr.deleteButton = widgets.NewQPushButton2("删除", ptr)
	ptr.upMoveButton = widgets.NewQPushButton2("上移", ptr)
	ptr.downMoveButton = widgets.NewQPushButton2("下移", ptr)

	ptr.copyButton.SetEnabled(false)
	ptr.deleteButton.SetEnabled(false)
	ptr.upMoveButton.SetEnabled(false)
	ptr.downMoveButton.SetEnabled(false)

	ptr.advancedFrame = widgets.NewQFrame(ptr, 0)
	ptr.advancedFrame.SetVisible(false)

	ptr.advancedCheckBox = widgets.NewQCheckBox2("高级设置", ptr)

	ptr.portEdit = widgets.NewQLineEdit(ptr.advancedFrame)
	ptr.clientIPEdit = widgets.NewQLineEdit(ptr.advancedFrame)
	ptr.domainsEdit = widgets.NewQLineEdit(ptr.advancedFrame)
	ptr.expectIPsEdit = widgets.NewQLineEdit(ptr.advancedFrame)

	ptr.portEdit.SetEnabled(false)
	ptr.clientIPEdit.SetEnabled(false)
	ptr.domainsEdit.SetEnabled(false)
	ptr.expectIPsEdit.SetEnabled(false)

	ptr.portEdit.SetPlaceholderText("端口")
	ptr.clientIPEdit.SetPlaceholderText("客户端IP")
	ptr.domainsEdit.SetPlaceholderText("域名(多个以,隔开)")
	ptr.expectIPsEdit.SetPlaceholderText("期待IP(多个以,隔开)")

	advancedLayout := widgets.NewQVBoxLayout2(ptr.advancedFrame)
	advancedLayout.AddWidget(ptr.advancedCheckBox, 0, core.Qt__AlignHCenter)
	advancedLayout.AddWidget(ptr.portEdit, 1, 0)
	advancedLayout.AddWidget(ptr.clientIPEdit, 1, 0)
	advancedLayout.AddWidget(ptr.domainsEdit, 1, 0)
	advancedLayout.AddWidget(ptr.expectIPsEdit, 1, 0)

	hboxLayout.AddWidget(ptr.listView, 1, 0)
	hboxLayout.AddWidget(ptr.advancedFrame, 1, 0)

	actionLayout := widgets.NewQHBoxLayout2(ptr)
	actionLayout.AddWidget(ptr.addButton, 0, core.Qt__AlignHCenter)
	actionLayout.AddWidget(ptr.copyButton, 0, core.Qt__AlignHCenter)
	actionLayout.AddWidget(ptr.deleteButton, 0, core.Qt__AlignHCenter)
	actionLayout.AddWidget(ptr.upMoveButton, 0, core.Qt__AlignHCenter)
	actionLayout.AddWidget(ptr.downMoveButton, 0, core.Qt__AlignHCenter)

	mainLayout.AddLayout(hboxLayout, 1)
	mainLayout.AddLayout(actionLayout, 1)

	ptr.SetLayout(mainLayout)

	ptr.initConnect()
}

func (ptr *DNSListWidget) initConnect() {
	ptr.listView.ConnectCurrentChanged(func(current *core.QModelIndex, previous *core.QModelIndex) {
		row := current.Row()

		ptr.copyButton.SetEnabled(true)
		ptr.deleteButton.SetEnabled(true)
		ptr.upMoveButton.SetEnabled(row != 0)
		ptr.downMoveButton.SetEnabled(row != len(ptr.listModel.modelData)-1)
		ptr.advancedFrame.SetVisible(true)

		model := ptr.listModel.modelData[row]
		ptr.currentDnsServer = model

		ptr.portEdit.SetText(strconv.Itoa(model.Port))
		ptr.clientIPEdit.SetText(model.ClientIp)
		ptr.domainsEdit.SetText(strings.Join(model.Domains, ","))
		ptr.expectIPsEdit.SetText(strings.Join(model.ExpectIPs, ","))

		ptr.advancedCheckBox.SetChecked(model.Port != 0)
		ptr.portEdit.SetEnabled(model.Port != 0)
		ptr.clientIPEdit.SetEnabled(model.Port != 0)
		ptr.domainsEdit.SetEnabled(model.Port != 0)
		ptr.expectIPsEdit.SetEnabled(model.Port != 0)
	})

	ptr.addButton.ConnectClicked(func(checked bool) {
		ptr.listModel.add(conf.DnsServer{Address: "1.1.1.1"})
	})

	ptr.deleteButton.ConnectClicked(func(checked bool) {
		indexes := ptr.listView.SelectedIndexes()
		if len(indexes) > 0 {
			ptr.listModel.remove(indexes[0].Row())
		}
	})

	ptr.copyButton.ConnectClicked(func(checked bool) {
		indexes := ptr.listView.SelectedIndexes()
		if len(indexes) > 0 {
			ptr.listModel.copy(indexes[0].Row())
		}
	})

	ptr.upMoveButton.ConnectClicked(func(checked bool) {
		indexes := ptr.listView.SelectedIndexes()
		if len(indexes) > 0 {
			ptr.listModel.UpMove(indexes[0].Row())
		}
	})

	ptr.downMoveButton.ConnectClicked(func(checked bool) {
		indexes := ptr.listView.SelectedIndexes()
		if len(indexes) > 0 {
			ptr.listModel.DownMove(indexes[0].Row())
		}
	})

	ptr.advancedCheckBox.ConnectClicked(func(checked bool) {
		if checked && ptr.portEdit.Text() == "" {
			ptr.portEdit.SetText("53")
		} else {
			ptr.portEdit.SetText("")
			if ptr.currentDnsServer != nil {
				ptr.currentDnsServer.Port = 0
			}
		}

		ptr.portEdit.SetEnabled(checked)
		ptr.clientIPEdit.SetEnabled(checked)
		ptr.domainsEdit.SetEnabled(checked)
		ptr.expectIPsEdit.SetEnabled(checked)
	})

	ptr.portEdit.ConnectTextChanged(func(text string) {
		if ptr.currentDnsServer != nil {
			if port, err := strconv.Atoi(text); err == nil {
				ptr.currentDnsServer.Port = port
			}
		}
	})

	ptr.clientIPEdit.ConnectTextChanged(func(text string) {
		if ptr.currentDnsServer != nil {
			ptr.currentDnsServer.ClientIp = text
		}
	})

	ptr.domainsEdit.ConnectTextChanged(func(text string) {
		if ptr.currentDnsServer != nil {
			if text == "" {
				ptr.currentDnsServer.Domains = []string{}
			} else {
				ptr.currentDnsServer.Domains = strings.Split(text, ",")
			}
		}
	})

	ptr.expectIPsEdit.ConnectTextChanged(func(text string) {
		if ptr.currentDnsServer != nil {
			if text == "" {
				ptr.currentDnsServer.ExpectIPs = []string{}
			} else {
				ptr.currentDnsServer.ExpectIPs = strings.Split(text, ",")
			}
		}
	})
}

func (ptr *DNSListWidget) LoadData(servers []conf.DnsServer) {
	for _, server := range servers {
		ptr.listModel.Add(server)
	}
}

func (ptr *DNSListWidget) Data() (servers []conf.DnsServer) {
	for _, item := range ptr.listModel.modelData {
		servers = append(servers, *item)
	}

	return
}
