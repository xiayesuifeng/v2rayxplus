package widgets

import (
	"github.com/therecipe/qt/core"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
)

type SubscriptionTableModel struct {
	core.QAbstractTableModel

	_ func() `constructor:"init"`

	_ func(row int)                `signal:"remove,auto"`
	_ func(item conf.Subscription) `signal:"add,auto"`

	modelData []conf.Subscription
}

func (m *SubscriptionTableModel) init() {
	m.ConnectHeaderData(m.headerData)
	m.ConnectRowCount(m.rowCount)
	m.ConnectColumnCount(m.columnCount)
	m.ConnectData(m.data)
	m.ConnectSetData(m.setData)

	m.ConnectFlags(func(index *core.QModelIndex) core.Qt__ItemFlag {
		flags := core.Qt__ItemIsSelectable | core.Qt__ItemIsEnabled
		if index.Column() == 3 {
			flags |= core.Qt__ItemIsUserCheckable
		} else {
			flags |= core.Qt__ItemIsEditable
		}

		return flags
	})
}

func (m *SubscriptionTableModel) setData(index *core.QModelIndex, value *core.QVariant, role int) bool {
	switch role {
	case int(core.Qt__EditRole):
		switch index.Column() {
		case 0:
			m.modelData[index.Row()].Group = value.ToString()
		case 1:
			m.modelData[index.Row()].URL = value.ToString()
		case 2:
			m.modelData[index.Row()].Adapter = value.ToString()
		}
	case int(core.Qt__CheckStateRole):
		if index.Column() == 3 {
			m.modelData[index.Row()].UpdateOnStart = value.ToBool()
		}
	}

	m.DataChanged(index, index, []int{role})

	return true
}

func (m *SubscriptionTableModel) headerData(section int, orientation core.Qt__Orientation, role int) *core.QVariant {
	if role != int(core.Qt__DisplayRole) || orientation == core.Qt__Vertical {
		return m.HeaderDataDefault(section, orientation, role)
	}

	switch section {
	case 0:
		return core.NewQVariant1("分组名")
	case 1:
		return core.NewQVariant1("URL")
	case 2:
		return core.NewQVariant1("适配器")
	case 3:
		return core.NewQVariant1("启动时更新")
	}
	return core.NewQVariant()
}

func (m *SubscriptionTableModel) rowCount(*core.QModelIndex) int {
	return len(m.modelData)
}

func (m *SubscriptionTableModel) columnCount(*core.QModelIndex) int {
	return 4
}

func (m *SubscriptionTableModel) data(index *core.QModelIndex, role int) *core.QVariant {
	item := m.modelData[index.Row()]

	switch role {
	case int(core.Qt__DisplayRole):
		fallthrough
	case int(core.Qt__EditRole):
		switch index.Column() {
		case 0:
			return core.NewQVariant1(item.Group)
		case 1:
			return core.NewQVariant1(item.URL)
		case 2:
			return core.NewQVariant1(item.Adapter)
		}
	case int(core.Qt__CheckStateRole):
		if index.Column() == 3 {
			if item.UpdateOnStart {
				return core.NewQVariant1(core.Qt__Checked)
			} else {
				return core.NewQVariant1(core.Qt__Unchecked)

			}
		}
	}

	return core.NewQVariant()
}

func (m *SubscriptionTableModel) remove(row int) {
	if len(m.modelData) == 0 {
		return
	}
	m.BeginRemoveRows(core.NewQModelIndex(), row, row)
	if row == len(m.modelData)-1 {
		m.modelData = m.modelData[:row]
	} else {
		m.modelData = append(m.modelData[:row], m.modelData[row+1:]...)
	}
	m.EndRemoveRows()
}

func (m *SubscriptionTableModel) add(item conf.Subscription) {
	m.BeginInsertRows(core.NewQModelIndex(), len(m.modelData), len(m.modelData))
	m.modelData = append(m.modelData, item)
	m.EndInsertRows()
}

func (m *SubscriptionTableModel) copy(row int) {
	m.BeginInsertRows(core.NewQModelIndex(), len(m.modelData), len(m.modelData))
	m.modelData = append(m.modelData, m.modelData[row])
	m.EndInsertRows()
}
