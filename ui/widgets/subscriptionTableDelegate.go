package widgets

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/xiayesuifeng/v2rayxplus/subscription"
)

type SubscriptionTableDelegate struct {
	widgets.QItemDelegate

	adapters []string

	_ func() `constructor:"init"`
}

func (d *SubscriptionTableDelegate) init() {
	d.adapters = subscription.GetAdapterNames()

	d.ConnectCreateEditor(d.createEditor)
	d.ConnectSetEditorData(d.setEditorData)
	d.ConnectSetModelData(func(editor *widgets.QWidget, model *core.QAbstractItemModel, index *core.QModelIndex) {
		comboBox := widgets.NewQComboBoxFromPointer(editor.Pointer())
		model.SetData(index, core.NewQVariant1(comboBox.CurrentText()), int(core.Qt__EditRole))

	})
	d.ConnectUpdateEditorGeometry(func(editor *widgets.QWidget, option *widgets.QStyleOptionViewItem, index *core.QModelIndex) {
		editor.SetGeometry(option.Rect())
		widgets.NewQComboBoxFromPointer(editor.Pointer()).ShowPopup()
	})
}

func (d *SubscriptionTableDelegate) createEditor(parent *widgets.QWidget, option *widgets.QStyleOptionViewItem, index *core.QModelIndex) *widgets.QWidget {
	comboBox := widgets.NewQComboBox(parent)
	comboBox.AddItems(d.adapters)
	return comboBox.QWidget_PTR()
}

func (d *SubscriptionTableDelegate) setEditorData(editor *widgets.QWidget, index *core.QModelIndex) {
	value := index.Model().Data(index, int(core.Qt__EditRole)).ToString()
	widgets.NewQComboBoxFromPointer(editor.Pointer()).SetCurrentText(value)
}
