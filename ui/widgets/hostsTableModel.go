package widgets

import (
	"github.com/therecipe/qt/core"
)

type HostsTableItem struct {
	Host string
	IP   string
}

type HostsTableModel struct {
	core.QAbstractTableModel

	_ func() `constructor:"init"`

	_ func(row int)             `signal:"remove,auto"`
	_ func(item HostsTableItem) `signal:"add,auto"`
	_ func(row int)             `signal:"copy,auto"`

	modelData []HostsTableItem
}

func (m *HostsTableModel) init() {
	m.ConnectHeaderData(m.headerData)
	m.ConnectRowCount(m.rowCount)
	m.ConnectColumnCount(m.columnCount)
	m.ConnectData(m.data)
	m.ConnectSetData(m.setData)

	m.ConnectFlags(func(index *core.QModelIndex) core.Qt__ItemFlag {
		return core.Qt__ItemIsSelectable | core.Qt__ItemIsEnabled | core.Qt__ItemIsEditable
	})
}

func (m *HostsTableModel) setData(index *core.QModelIndex, value *core.QVariant, role int) bool {
	if role != int(core.Qt__EditRole) {
		return false
	}

	switch index.Column() {
	case 0:
		m.modelData[index.Row()].Host = value.ToString()
	case 1:
		m.modelData[index.Row()].IP = value.ToString()
	}
	m.DataChanged(index, index, []int{role})

	return true
}

func (m *HostsTableModel) headerData(section int, orientation core.Qt__Orientation, role int) *core.QVariant {
	if role != int(core.Qt__DisplayRole) || orientation == core.Qt__Vertical {
		return m.HeaderDataDefault(section, orientation, role)
	}

	switch section {
	case 0:
		return core.NewQVariant1("域名")
	case 1:
		return core.NewQVariant1("IP")
	}
	return core.NewQVariant()
}

func (m *HostsTableModel) rowCount(*core.QModelIndex) int {
	return len(m.modelData)
}

func (m *HostsTableModel) columnCount(*core.QModelIndex) int {
	return 2
}

func (m *HostsTableModel) data(index *core.QModelIndex, role int) *core.QVariant {
	if role != int(core.Qt__DisplayRole) && role != int(core.Qt__EditRole) {
		return core.NewQVariant()
	}

	item := m.modelData[index.Row()]
	switch index.Column() {
	case 0:
		return core.NewQVariant1(item.Host)
	case 1:
		return core.NewQVariant1(item.IP)
	}

	return core.NewQVariant()
}

func (m *HostsTableModel) remove(row int) {
	if len(m.modelData) == 0 {
		return
	}
	m.BeginRemoveRows(core.NewQModelIndex(), row, row)
	if row == len(m.modelData)-1 {
		m.modelData = m.modelData[:row]
	} else {
		m.modelData = append(m.modelData[:row], m.modelData[row+1:]...)
	}
	m.EndRemoveRows()
}

func (m *HostsTableModel) add(item HostsTableItem) {
	m.BeginInsertRows(core.NewQModelIndex(), len(m.modelData), len(m.modelData))
	m.modelData = append(m.modelData, item)
	m.EndInsertRows()
}

func (m *HostsTableModel) copy(row int) {
	m.BeginInsertRows(core.NewQModelIndex(), len(m.modelData), len(m.modelData))
	m.modelData = append(m.modelData, m.modelData[row])
	m.EndInsertRows()
}
