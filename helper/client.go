package helper

import (
	"github.com/godbus/dbus/v5"
)

type Client struct {
	conn      *dbus.Conn
	busObject dbus.BusObject
}

func NewClient() (*Client, error) {
	conn, err := dbus.SystemBus()
	if err != nil {
		return nil, err
	}

	return &Client{
		conn:      conn,
		busObject: conn.Object(InterfaceName, "/me/firerain/v2rayxplus"),
	}, nil
}

func (c *Client) StartService(bytes []byte) error {
	return c.busObject.Call(InterfaceName+"."+"StartService", 0, bytes).Err
}

func (c *Client) RestartService(bytes []byte) error {
	return c.busObject.Call(InterfaceName+"."+"RestartService", 0, bytes).Err
}

func (c *Client) StopService() error {
	return c.busObject.Call(InterfaceName+"."+"StopService", 0).Err
}

func (c *Client) StopHelper() error {
	return c.busObject.Call(InterfaceName+"."+"StopHelper", 0).Err
}
