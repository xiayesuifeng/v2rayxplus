package conf

import (
	"encoding/json"
	"strings"
)

type DnsConfig struct {
	Hosts   map[string]string `json:"hosts,omitempty"`
	Servers []DnsServer       `json:"servers"`
}

type DnsServer struct {
	Address   string   `json:"address,omitempty"`
	Port      int      `json:"port,omitempty"`
	ClientIp  string   `json:"clientIp,omitempty"`
	Domains   []string `json:"domains,omitempty"`
	ExpectIPs []string `json:"expectIPs,omitempty"`
}

func NewDnsServerForAddress(address string) (dnsServer DnsServer) {
	dnsServer.Address = address
	return
}

func (d *DnsServer) MarshalJSON() ([]byte, error) {
	if d.Port == 0 {
		return json.Marshal(&d.Address)
	} else {
		type DnsServerAlias DnsServer

		alias := struct {
			*DnsServerAlias
		}{
			DnsServerAlias: (*DnsServerAlias)(d),
		}

		return json.Marshal(&alias)
	}
}

func (d *DnsServer) UnmarshalJSON(bytes []byte) error {
	str := string(bytes)
	if strings.Contains(str, "address") {
		type DnsServerAlias DnsServer

		alias := struct {
			*DnsServerAlias
		}{
			DnsServerAlias: (*DnsServerAlias)(d),
		}

		if err := json.Unmarshal(bytes, &alias); err != nil {
			return err
		}
	} else {
		if err := json.Unmarshal(bytes, &d.Address); err != nil {
			return err
		}
	}

	return nil
}
