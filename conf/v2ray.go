package conf

import (
	"encoding/json"
	"os"
)

type OutboundConfig struct {
	Protocol      string          `json:"protocol"`
	Tag           string          `json:"tag,omitempty"`
	Settings      json.RawMessage `json:"settings,omitempty"`
	StreamSetting *StreamConfig   `json:"streamSettings,omitempty"`
	MuxSettings   *MuxConfig      `json:"mux,omitempty"`
}

type InboundConfig struct {
	Port           *uint           `json:"port"`
	Protocol       string          `json:"protocol"`
	StreamSetting  *StreamConfig   `json:"streamSettings,omitempty"`
	Settings       json.RawMessage `json:"settings,omitempty"`
	Tag            string          `json:"tag,omitempty"`
	DomainOverride *[]string       `json:"domainOverride,omitempty"`
	Sniffing       *SniffingConfig `json:"sniffing,omitempty"`
}

type SniffingConfig struct {
	Enabled      bool     `json:"enabled"`
	DestOverride []string `json:"destOverride"`
}

type SocketConfig struct {
	Mark   int32  `json:"mark,omitempty"`
	TFO    *bool  `json:"tcpFastOpen,omitempty"`
	TProxy string `json:"tproxy,omitempty"`
}

type MuxConfig struct {
	Enabled     bool   `json:"enabled,omitempty"`
	Concurrency uint16 `json:"concurrency,omitempty"`
}

type V2rayConfig struct {
	V2rayXPlus         V2rayXPlusConfig  `json:"v2rayXPlus,omitempty"`
	Port               uint16            `json:"port,omitempty"`
	RouterConfig       *RouterConfig     `json:"routing"`
	DNSConfig          *DnsConfig        `json:"dns"`
	InboundConfigList  []*InboundConfig  `json:"inbounds"`
	OutboundConfigList []*OutboundConfig `json:"outbounds,omitempty"`
}

func ParseV2ray(conf string) (*V2rayConfig, error) {
	confJson, err := os.Open(conf)
	if err != nil {
		return nil, err
	}

	v2rayConfig := new(V2rayConfig)
	if err = json.NewDecoder(confJson).Decode(v2rayConfig); err != nil {
		return nil, err
	}

	return v2rayConfig, nil
}

func (v2ray *V2rayConfig) Save(conf string) error {
	confJson, err := os.OpenFile(conf, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
	if err != nil {
		return err
	}

	return json.NewEncoder(confJson).Encode(v2ray)
}

func NewV2rayConfig() *V2rayConfig {
	v2ray := &V2rayConfig{}

	tproxy := Conf.TProxy
	if tproxy == "" {
		tproxy = "redirect"
	}

	port := uint(Conf.ListerPort)
	dokodemoConfig := &InboundConfig{Port: &port, Tag: "transparent"}
	dokodemoConfig.DomainOverride = &[]string{"tls", "http"}
	dokodemoConfig.Protocol = "dokodemo-door"
	dokodemoConfig.Settings = []byte(`{"network": "tcp,udp","followRedirect": true}`)
	dokodemoConfig.Sniffing = &SniffingConfig{true, []string{"http", "tls"}}
	dokodemoConfig.StreamSetting = &StreamConfig{SocketSettings: &SocketConfig{TProxy: tproxy}}

	freedomConfig := &OutboundConfig{}
	freedomConfig.Protocol = "freedom"
	freedomConfig.Tag = "direct"
	freedomConfig.Settings = []byte(`{"domainStrategy": "UseIP"}`)
	freedomConfig.StreamSetting = &StreamConfig{SocketSettings: &SocketConfig{Mark: 255}}

	blackholeConfig := &OutboundConfig{}
	blackholeConfig.Protocol = "blackhole"
	blackholeConfig.Tag = "block"
	blackholeConfig.Settings = []byte(`{"response": {"type": "http"}}`)

	var rules []RuleConfig

	if Conf.HijackDNS {
		rules = append(rules, RuleConfig{Type: "field", InboundTag: []string{"transparent"}, Port: 53, Network: "udp", OutboundTag: "dns-out"})
		rules = append(rules, RuleConfig{Type: "field", InboundTag: []string{"transparent"}, Port: 123, Network: "udp", OutboundTag: "direct"})
	}

	adsRule := RuleConfig{Type: "field", Domain: []string{"geosite:category-ads-all"}, OutboundTag: Conf.AdsOutboundTag}
	btRule := RuleConfig{Type: "field", Protocol: []string{"bittorrent"}, OutboundTag: Conf.BTOutboundTag}
	ipRule := RuleConfig{Type: "field", IP: []string{"geoip:cn", "geoip:private"}, OutboundTag: Conf.IPOutboundTag}
	siteRule := RuleConfig{Type: "field", Domain: []string{"geosite:cn"}, OutboundTag: Conf.SiteOutboundTag}

	rules = append(rules, ipRule, siteRule, adsRule, btRule)

	if len(Conf.DomainWhitelist) > 0 || len(Conf.IPWhitelist) > 0 {
		rules = append(rules, RuleConfig{Type: "field", Domain: Conf.DomainWhitelist, IP: Conf.IPWhitelist, OutboundTag: "direct"})
	}

	if len(Conf.DomainBlacklist) > 0 || len(Conf.IPBlacklist) > 0 {
		rules = append(rules, RuleConfig{Type: "field", Domain: Conf.DomainBlacklist, IP: Conf.IPBlacklist, OutboundTag: "proxy"})
	}

	routerConfig := &RouterConfig{}
	routerConfig.Settings = &RouterRulesConfig{DomainStrategy: "IPIfNonMatch", RuleList: rules}

	serverOutboundConfig := &OutboundConfig{Tag: "proxy", Settings: []byte("{}"), StreamSetting: &StreamConfig{SocketSettings: &SocketConfig{Mark: 255}}}

	v2ray.DNSConfig = &DnsConfig{Servers: Conf.DnsServers, Hosts: Conf.Hosts}
	v2ray.InboundConfigList = append(v2ray.InboundConfigList, dokodemoConfig)
	v2ray.OutboundConfigList = append(v2ray.OutboundConfigList, serverOutboundConfig)
	v2ray.OutboundConfigList = append(v2ray.OutboundConfigList, freedomConfig)
	v2ray.OutboundConfigList = append(v2ray.OutboundConfigList, blackholeConfig)
	v2ray.RouterConfig = routerConfig
	v2ray.V2rayXPlus.IptablesDirectIPList = Conf.IptablesDirectIPList

	if Conf.HijackDNS {
		v2ray.OutboundConfigList = append(v2ray.OutboundConfigList, &OutboundConfig{
			Tag:           "dns-out",
			Protocol:      "dns",
			StreamSetting: &StreamConfig{SocketSettings: &SocketConfig{Mark: 255}},
		})
	}

	return v2ray
}
