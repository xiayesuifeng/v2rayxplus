package conf

import (
	"encoding/json"
	"errors"
	"gitlab.com/xiayesuifeng/v2rayxplus/helper"
	"io"
	"os"
)

var (
	ConfigPath             string
	V2rayConfigPath        string
	V2rayDefaultConfigPath string

	Conf         *Config
	HelperClient *helper.Client

	Session *SessionConfig
)

type Config struct {
	Theme                string            `json:"theme"`
	TProxy               string            `json:"tproxy"`
	ListerPort           int               `json:"lister_port"`
	DnsServers           []DnsServer       `json:"dnsServers"`
	AdsOutboundTag       string            `json:"adsOutboundTag"`
	BTOutboundTag        string            `json:"btOutboundTag"`
	IPOutboundTag        string            `json:"ipOutboundTag"`
	SiteOutboundTag      string            `json:"siteOutboundTag"`
	DomainWhitelist      []string          `json:"domainWhitelist"`
	DomainBlacklist      []string          `json:"domainBlacklist"`
	IPWhitelist          []string          `json:"ipWhitelist"`
	IPBlacklist          []string          `json:"ipBlacklist"`
	IptablesDirectIPList []string          `json:"iptablesDirectIPList"`
	HijackDNS            bool              `json:"hijackDNS"`
	Hosts                map[string]string `json:"hosts"`
	Subscriptions        []Subscription    `json:"subscriptions"`
}

type Subscription struct {
	Group         string `json:"group"`
	URL           string `json:"url"`
	Adapter       string `json:"adapter"`
	UpdateOnStart bool   `json:"updateOnStart"`
}

type V2rayXPlusConfig struct {
	HijackDNS            bool     `json:"-"`
	Port                 int      `json:"-"`
	TProxy               string   `json:"-"`
	IptablesDirectIPList []string `json:"iptablesDirectIPList"`
}

func (c *Config) SaveConf() error {
	file, err := os.OpenFile(ConfigPath+"/config.json", os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}

	if err := json.NewEncoder(file).Encode(c); err != nil {
		return err
	}

	return nil
}

type SessionConfig struct {
	Config string `json:"config"`
	Group  string `json:"group"`
}

func LoadSession() error {
	file, err := os.OpenFile(ConfigPath+"/session.json", os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return err
	}
	defer file.Close()

	Session = &SessionConfig{}
	if err := json.NewDecoder(file).Decode(Session); err != nil && !errors.Is(err, io.EOF) {
		return err
	}

	return nil
}

func (s *SessionConfig) Save() error {
	file, err := os.OpenFile(ConfigPath+"/session.json", os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}

	if err := json.NewEncoder(file).Encode(s); err != nil {
		return err
	}

	return nil
}
