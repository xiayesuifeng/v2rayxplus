module gitlab.com/xiayesuifeng/v2rayxplus

go 1.16

require (
	github.com/TheCreeper/go-notify v0.2.0
	github.com/fsnotify/fsnotify v1.4.9
	github.com/godbus/dbus/v5 v5.0.3
	github.com/therecipe/qt v0.0.0-20200904063919-c0c124a5770d
	github.com/xiayesuifeng/go-polkit v0.0.0-20210318090312-fee8c0fda35b
)
